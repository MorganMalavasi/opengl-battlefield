#version 410 core 

out vec4 FragColor;

in vec2 TexCoords;

uniform float exposure;
uniform bool hdr;
uniform sampler2D scene;
uniform sampler2D bloomBlur;

void main ()
{   

    // -----------------------------------
    // we sum up the values of the two texture to determine the final color for the fragment 
    vec3 hdrColor = texture(scene, TexCoords).rgb;
    vec3 bloomColor = texture(bloomBlur, TexCoords).rgb;
    hdrColor += bloomColor;
    // -----------------------------------
    // but before we apply hdr color formula 
    vec3 result = vec3(1.0) - exp(-hdrColor * exposure);
    // -----------------------------------
    FragColor = vec4(result, 1.0);
}