#version 410 core 
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in uvec4 inSkinBones;
layout (location = 3) in vec4 inSkinWeights;
layout (location = 4) in vec2 aTexCoords;

uniform mat4 uMatrixPalette[96];

uniform mat4 lightSpaceMatrix;
uniform mat4 modelMatrix;

void main ()
{
    vec4 pos = vec4(aPos, 1.0);
	
	// Skin the position
	vec4 skinnedPos = inSkinWeights.x * (uMatrixPalette[inSkinBones.x] * pos );
	skinnedPos += inSkinWeights.y * (uMatrixPalette[inSkinBones.y] * pos);
	skinnedPos += inSkinWeights.z * (uMatrixPalette[inSkinBones.z] * pos);
	skinnedPos += inSkinWeights.w * (uMatrixPalette[inSkinBones.w] * pos);


    gl_Position = lightSpaceMatrix * modelMatrix * skinnedPos;
}