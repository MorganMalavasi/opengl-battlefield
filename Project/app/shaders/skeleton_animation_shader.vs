#version 410 core 

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in uvec4 inSkinBones;
layout (location = 3) in vec4 inSkinWeights;
layout (location = 4) in vec2 aTexCoords;

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

uniform mat4 uMatrixPalette[96];

out vec3 Normal;
out vec3 FragPos;
out vec2 TexCoords;

void main (){

	vec4 pos = vec4(position, 1.0);
	
	// Skin the position
	vec4 skinnedPos = inSkinWeights.x * (uMatrixPalette[inSkinBones.x] * pos );
	skinnedPos += inSkinWeights.y * (uMatrixPalette[inSkinBones.y] * pos);
	skinnedPos += inSkinWeights.z * (uMatrixPalette[inSkinBones.z] * pos);
	skinnedPos += inSkinWeights.w * (uMatrixPalette[inSkinBones.w] * pos);

    gl_Position = projectionMatrix * viewMatrix * modelMatrix * skinnedPos;
    // Normal = normalize(normalMatrix * aNormal);
    Normal = mat3(transpose(inverse(modelMatrix))) * aNormal;

	vec4 skinnedNormal = vec4(Normal, 0.0f);
	skinnedNormal = inSkinWeights.x * (uMatrixPalette[inSkinBones.x] * skinnedNormal)
		+ inSkinWeights.y * (uMatrixPalette[inSkinBones.y] * skinnedNormal)
		+ inSkinWeights.z * (uMatrixPalette[inSkinBones.z] * skinnedNormal)
		+ inSkinWeights.w * (uMatrixPalette[inSkinBones.w] * skinnedNormal);

    FragPos = vec3(modelMatrix * vec4(position, 1.0));
    TexCoords = aTexCoords;
}

