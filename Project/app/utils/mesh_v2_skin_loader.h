/*
    PROJECT -> REAL TIME GRAPHICS PROGRAMMING 
    AUTHOR -> MORGAN MALAVASI 
    MAT. 960552

    The MeshSkin class is used for loading the basic model of the CatWarrior 
    RapidJSON is a JSON parser and generator for C++, we use it to extract vertex info 
    of the model.
*/

#pragma once

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <glad/glad.h>
#include <glfw/glfw3.h>
#include "rapidjson/document.h"
#include "mesh_v1.h"
#include "../include/glm/glm.hpp"

// data structure for vertices

namespace
{
    union Vertx {
        float f;
        uint8_t b[4];
    };
} // namespace

/////////////////// MeshSkin class ///////////////////////
class MeshSkin
{
public:
    unsigned int numVertices;
    unsigned int numIndices;

    // VAO
    GLuint VAO;

    // VBO and EBO
    GLuint VBO, EBO;

    //////////////////////////////////////////
    // Constructor
    MeshSkin(const std::string &filename)
    {

        // check if the file exists, if not found return error and display error
        std::ifstream file(filename);
        if (!file.is_open())
        {
            std::cout << "FIle not found: Mesh %s " + filename << std::endl;
            exit(-1);
        }

        // read buffer stream
        std::stringstream fileStream;
        fileStream << file.rdbuf();
        // convert in string 
        std::string contents = fileStream.str();
        rapidjson::StringStream jsonStr(contents.c_str());
        rapidjson::Document doc;
        doc.ParseStream(jsonStr);

        // check if the doc is an object
        if (!doc.IsObject())
        {
            std::cout << "Mesh " + filename + " not valid json " << std::endl;
            exit(-1);
        }

        size_t vertSize = 10;

        // Load vertices
        const rapidjson::Value &vertsJson = doc["vertices"];
        if (!vertsJson.IsArray() || vertsJson.Size() < 1)
        {
            std::cout << "Mesh has no vertices or is not Array" << std::endl;
            exit(-1);
        }

        std::vector<Vertx> mVertices;
        mVertices.reserve(vertsJson.Size() * vertSize);

        for (rapidjson::SizeType i = 0; i < vertsJson.Size(); i++)
        {
            // just assume we have 16 elements
            const rapidjson::Value &vert = vertsJson[i];
            // check every line of the mesh
            if (!vert.IsArray())
            {
                std::cout << "Unexpected vertex format for " + filename << std::endl;
                exit(-1);
            }

            // tmp 
            Vertx v;

            for (rapidjson::SizeType j = 0; j < 6; j++)
            {
                v.f = static_cast<float>(vert[j].GetDouble());
                mVertices.emplace_back(v);
            }

            // add skin info
            for (rapidjson::SizeType j = 6; j < 14; j += 4)
            {
                v.b[0] = vert[j].GetUint();
                v.b[1] = vert[j + 1].GetUint();
                v.b[2] = vert[j + 2].GetUint();
                v.b[3] = vert[j + 3].GetUint();
                mVertices.emplace_back(v);
            }

            // tex coords
            for (rapidjson::SizeType j = 14; j < vert.Size(); j++)
            {
                v.f = vert[j].GetDouble();
                mVertices.emplace_back(v);
            }

        }

        // Load the indices
        const rapidjson::Value &indJson = doc["indices"];
        if (!indJson.IsArray() || indJson.Size() < 1)
        {
            std::cout << "Mesh has no indices " << std::endl;
            exit(-1);
        }

        std::vector<unsigned int> mIndices;
        mIndices.reserve(indJson.Size() * 3);
        for (rapidjson::SizeType i = 0; i < indJson.Size(); i++)
        {
            const rapidjson::Value &ind = indJson[i];
            if (!ind.IsArray() || ind.Size() != 3)
            {
                std::cout << "Indices format incorrect " << std::endl;
                exit(-1);
            }

            // save the three values for the indices
            mIndices.emplace_back(ind[0].GetUint());
            mIndices.emplace_back(ind[1].GetUint());
            mIndices.emplace_back(ind[2].GetUint());
        }

        // once that we load all the vertex and the index
        // we are ready to load the mesh in memory

        LoadMesh(mVertices.data(), static_cast<unsigned>(mVertices.size()) / vertSize, mIndices.data(), static_cast<unsigned>(mIndices.size()));
    }

    void LoadMesh(const void *verts, unsigned int numVerts, const unsigned int *indices, unsigned int numIndices)
    {
        numVertices = numVerts;
        this->numIndices = numIndices;
        unsigned vertexS = 8 * sizeof(float) + 8 * sizeof(char);

        // initialization of OpenGL buffers
        // we create the buffers
        glGenVertexArrays(1, &VAO);
        // VAO is made "active"
        glBindVertexArray(VAO);

        glGenBuffers(1, &VBO);
        // we copy data in the VBO - we must set the data dimension, and the pointer to the structure cointaining the data
        glBindBuffer(GL_ARRAY_BUFFER, VBO);
        glBufferData(GL_ARRAY_BUFFER, numVerts * vertexS, verts, GL_STATIC_DRAW);

        glGenBuffers(1, &EBO);
        // we copy data in the EBO - we must set the data dimension, and the pointer to the structure cointaining the data
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, numIndices * sizeof(unsigned int), indices, GL_STATIC_DRAW);

        // we set in the VAO the pointers to the different vertex attributes (with the relative offsets inside the data structure)
        // vertex positions
        // these will be the positions to use in the layout qualifiers in the shaders ("layout (location = ...)"")
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, vertexS, 0);
        // Normals
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, vertexS, reinterpret_cast<void*>(sizeof(float) * 3));
        // Skinning indices 
        glEnableVertexAttribArray(2);
        glVertexAttribIPointer(2, 4, GL_UNSIGNED_BYTE, vertexS, reinterpret_cast<void*>(sizeof(float) * 6));
        // Skinning weights 
        glEnableVertexAttribArray(3);
        glVertexAttribPointer(3, 4, GL_UNSIGNED_BYTE, GL_TRUE, vertexS, reinterpret_cast<void*>(sizeof(float) * 6 + sizeof(char) * 4));
        // Texture Coordinates 
        glEnableVertexAttribArray(4);
        glVertexAttribPointer(4, 2, GL_FLOAT, GL_FALSE, vertexS, reinterpret_cast<void*>(sizeof(float) * 6 + sizeof(char) * 8));

        glBindVertexArray(0);
    }

    //////////////////////////////////////////

    // rendering of mesh
    void Draw()
    {
        // VAO is made "active"
        glBindVertexArray(VAO);
        // rendering of data in the VAO
        glDrawElements(GL_TRIANGLES, this->numIndices, GL_UNSIGNED_INT, nullptr);
        // VAO is "detached"
        glBindVertexArray(0);
    }

    
    void Delete()
    {
        glDeleteVertexArrays(1, &VAO);
        glDeleteBuffers(1, &VBO);
        glDeleteBuffers(1, &EBO);
    }
};